using RuoVea.ExFilter;
using RuoVea.QuartzNetUI.Authors;

var builder = WebApplication.CreateBuilder(args); 


builder.Services.AddControllersWithViews();
builder.Services.AddQuartzNetAuthorServer(builder.Configuration.GetSection("QuartzNetUI"));
builder.Services.Exception();

var app = builder.Build();


app.UseQuartzAuthorUI();
app.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
