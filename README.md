# QuartzNet Dashboard 任务管理界面

#### 介绍

  QuartzNet Dashboard界面-基于QuartzNetUI组件开发的任务管理界面

#### 源代码地址

https://gitee.com/starry123/ruovea-quartznet-ui.git

#### 使用说明

1、双击 QuartzNetDashboard.exe 文件

2、浏览器输入地址：http://localhost:5000/job

![](Doc/img/login.png)

3、输入账号密码 admin admin

周期作业

![周期作业](Doc/img/dashboard.png)

周期作业编辑

![周期作业编辑](Doc/img/edit.png)
![周期作业编辑](Doc/img/edit2.png)
![周期作业编辑](Doc/img/edit3.png)
![周期作业编辑](Doc/img/edit4.png)

执行日志

![执行日志](Doc/img/log.png)

作业分析

![作业分析](Doc/img/period.png)

设置

![设置](Doc/img/setting.png)

Info

![Info](Doc/img/guide.png)



![](Doc/img/dashboard.png)